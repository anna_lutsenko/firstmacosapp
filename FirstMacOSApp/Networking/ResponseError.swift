//
//  ResponseError.swift
//  FirstMacOSApp
//
//  Created by Anna on 5/22/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import Foundation

enum ResponseError: Error {
    case dataIsEmpty
    case parsingFailed
    
    var localizedDescription: String {
        switch self {
        case .dataIsEmpty:
            return "Data is empty"
        case .parsingFailed:
            return "Something went wrong"
        }
    }
}
