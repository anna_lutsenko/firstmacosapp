//
//  JSONRequestManager.swift
//  FirstMacOSApp
//
//  Created by Anna on 5/22/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import Foundation
import Alamofire

typealias JSONResponseCallback = (Swift.Result<WebResponseModel?, Error>) -> Void

class JSONRequestManager {
    public init () { }
    
    func genericRequest<T: Codable>(
        decode type: T.Type,
        parameters: Parameters? = nil,
        apiClient: APIClient,
        completion: @escaping (Swift.Result<T, Error>) -> Void) {
        
        apiClient.run(decode: type,
                      parameters: parameters) { (response) in
                        
                        switch response.result {
                        case .success(_):
                            do {
                                guard let data = response.data else { throw ResponseError.dataIsEmpty }
                                let object = try JSONDecoder().decode(type, from: data)
                                completion(.success(object))
                            } catch let error {
                                print("Parse locations error =  \(error)")
                                completion(.failure(error))
                            }
                            
                        case .failure(let error):
                            completion(.failure(error))
                        }
                        
        }
    }
    
    func getJSON(completion: @escaping JSONResponseCallback) {
//        let apiClient = APIClient().post()
//        genericRequest(decode: WebResponseModel.self,
//                       apiClient: apiClient,
//                       completion: completion)
        
        do {
            let json = try self.getJSON()
            completion(.success(json))
        } catch let error {
            completion(.failure(error))
        }
        
    }
    
    func getJSON() throws -> WebResponseModel? {
        let base64Str = "eyJzaCI6ICJkYXRlID4gL3RtcC9pbnRlcnZpZXcudHh0IiwgIm51bSI6IDMwLCAiSFRNTCI6ICI8IURPQ1RZUEUgaHRtbD48aHRtbD48aGVhZD48dGl0bGU+SW50ZXJ2aWV3IFByb2plY3Q8L3RpdGxlPjwvaGVhZD48Ym9keT5Hb29kIEludGVydmlldyBQcm9qZWN0IG5hbWU6IEFubmEgTHV0c2Vua28gZGF0ZTogb3BzPC9ib2R5PjwvaHRtbD4iLCAiYW5zIjogImdvb2RfYW5zd2VyIn0="
        
        guard let decodeData = Data(base64Encoded: base64Str) else { return nil }
        return try JSONDecoder().decode(WebResponseModel.self, from: decodeData)
    }
}
