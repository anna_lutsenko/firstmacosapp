//
//  APIConstants.swift
//  FirstMacOSApp
//
//  Created by Anna on 5/22/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import Foundation

struct APIConstants {
    private init() {}
    
    static let baseURL = "http://www.ifsac.pw/mc/in"
    
    struct Keys {
        static let name = "name"
        static let date = "date"
    }
}
