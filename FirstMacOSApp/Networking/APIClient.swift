//
//  APIClient.swift
//  FirstMacOSApp
//
//  Created by Anna on 5/22/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import Foundation
import Alamofire

class APIClient {
    var baseURL = APIConstants.baseURL
    private var method: HTTPMethod = .get
    
    private var defaultParameters: Parameters {
        return [APIConstants.Keys.name: "Anna Lutsenko",
                APIConstants.Keys.date: DateFormatter.shortDate.string(from: Date())]
    }
    
    func get() -> Self {
        self.method = .get
        return self
    }
    
    func post() -> Self {
        self.method = .post
        return self
    }
    
    func run<T: Decodable>(decode type: T.Type,
                           parameters: Parameters?,
                           completionHandler: @escaping (DataResponse<Any>) -> Void) {
        var param = defaultParameters
        if let additionalParam = parameters {
            param = param.merging(additionalParam) { (_, new) in new }
        }
        
        Alamofire.request(baseURL,
                          method: method,
                          parameters: param)
            .responseJSON(completionHandler: completionHandler)
    }
}
