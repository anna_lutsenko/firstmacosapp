//
//  WebResponseModel.swift
//  FirstMacOSApp
//
//  Created by Anna on 5/22/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import Foundation

struct WebResponseModel: Codable {
    let sh: String
    let number: Int?
    let htmlCode: String
    let answer: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        sh = try container.decode(String.self, forKey: .sh)
        number = try? container.decode(Int.self, forKey: .number)
        htmlCode = try container.decode(String.self, forKey: .htmlCode)
        answer = try? container.decode(String.self, forKey: .answer)
    }
}

extension WebResponseModel {
    enum CodingKeys: String, CodingKey {
        case sh
        case number = "num"
        case htmlCode = "HTML"
        case answer = "ans"
    }
}
