//
//  DateFormatter.swift
//  FirstMacOSApp
//
//  Created by Anna on 5/22/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import Foundation

extension DateFormatter {
    static let shortDate: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        return formatter
    }()
}
